class UrlProvider {
  static final BASE_URL = ("https://ggallery-app.herokuapp.com/api/");

  //GAMES
  static final BASE_GAMES = Uri.parse(BASE_URL + 'videojuegos');

  //NEWS
  static final BASE_NEWS = Uri.parse(BASE_URL + 'news');

  //COMMIT
  static final BASE_COMMIT = Uri.parse(BASE_URL + "commit");

  //REQUIREMET
  static final BASE_REQUIREMET = Uri.parse(BASE_URL + "requirements");

  //TAG
  static final BASE_TAG = Uri.parse(BASE_URL + "tags");

  //MAP/MARKETS
  static final BASE_MAP = Uri.parse(BASE_URL + 'map/markets');

  //PORPULARES/GAMES
  static final BASE_POPULAR =
      Uri.parse(BASE_URL + 'videojuegos/populares/games');

  //RECIENTES/GAMES
  static final BASE_RECIENTES =
      Uri.parse(BASE_URL + 'videojuegos/recientes/games');

  //VIDEOJUEGOS-MARKETS
  static final BASE_VIDEOJUEGOS_MARKETS =
      Uri.parse(BASE_URL + 'videojuegos-markets');
}
