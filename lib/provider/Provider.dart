import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

class DataProvider {
  Future getData(Uri url) async {
    var client = http.Client();
    var data = await client.get(url);
    return json.decode(data.body);
  }

  Future postDataUrl(Uri url, String payload) async {
    var client = http.Client();
    var data = await client.post(url, body: payload, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    return json.decode(data.body);
  }

  Future putData(Uri url, String payload) async {
    var client = http.Client();
    var data = await client.put(url, body: payload);
    return json.decode(data.body);
  }

  Future deleteData(Uri url) async {
    var client = http.Client();
    var data = await client.delete(url);
    return json.decode(data.body);
  }
}
