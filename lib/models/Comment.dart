import 'dart:convert';
import 'dart:core';

class Comment {
  String userName = "";
  String photo = "";
  double rating = 0.0;
  String comment = "";
  String idjuego = "";

  Comment(
      {required this.comment,
      required this.rating,
      required this.userName,
      required this.photo,
      required this.idjuego});

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
        userName: json['Usuario'],
        photo: json['Foto'],
        rating: json['Puntuacion'].toDouble(),
        comment: json['Comentario'],
        idjuego: json['JuegoId']);
  }

  String toJSON() {
    var data = jsonEncode({
      "Comentario": comment,
      "Puntuacion": rating,
      "Usuario": userName,
      "Foto": photo,
      "JuegoId": idjuego
    });
    return data;
  }
}
