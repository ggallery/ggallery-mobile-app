import 'dart:core';

class StoreMarkers {
  String idStore = "";
  String nombre = "";
  String correo = "";
  String telefono = "";
  String direccion = "";
  double latitude = 0.0;
  double longitude = 0.0;

  StoreMarkers({
    required this.idStore,
    required this.nombre,
    required this.correo,
    required this.telefono,
    required this.direccion,
    required this.latitude,
    required this.longitude,
  });

  factory StoreMarkers.fromJson(Map<String, dynamic> json) {
    return StoreMarkers(
        idStore: json['IdStore'],
        nombre: json['Nombre'],
        correo: json['Correo'],
        telefono: json['Telefono'],
        direccion: json['Direccion'],
        latitude: json['Latitude'],
        longitude: json['Longitude']);
  }
}
