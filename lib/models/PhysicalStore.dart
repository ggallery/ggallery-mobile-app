import 'dart:core';

class PhysicalStore {
  String nombre = "";
  String correo = "";
  String telefono = "";
  String direccion = "";
  String pagina = "";
  String ubicacion = "";

  PhysicalStore(
      {required this.nombre,
      required this.correo,
      required this.telefono,
      required this.direccion,
      required this.pagina,
      required this.ubicacion});

  factory PhysicalStore.fromJson(Map<String, dynamic> json) {
    return PhysicalStore(
        nombre: json['nombre'],
        correo: json['correo'],
        telefono: json['telefono'],
        direccion: json['direccion'],
        pagina: json['pagina'],
        ubicacion: json['ubicacion']);
  }
}
