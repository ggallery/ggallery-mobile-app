import 'dart:core';

class Store {
  String nombre = "";
  String correo = "";
  String tel = "";
  String direccion = "";
  String pagina = "";
  String logo = "";

  Store({
    required this.nombre,
    required this.correo,
    required this.tel,
    required this.direccion,
    required this.pagina,
    required this.logo,
  });

  factory Store.fromJson(Map<String, dynamic> json) {
    return Store(
        nombre: json['Nombre'],
        correo: json['Correo'],
        tel: json['Tel'],
        direccion: json['Direccion'],
        pagina: json['Pagina'],
        logo: json['Logo']);
  }
}
