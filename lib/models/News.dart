import 'dart:core';

class News {
  String titulo = "";
  String imagen = "";
  String contenido = "";
  String fecha = "";

  News(
      {required this.titulo,
      required this.imagen,
      required this.contenido,
      required this.fecha});

  factory News.fromJson(Map<String, dynamic> json) {
    return News(
        titulo: json['titulo'],
        imagen: json['imagen'],
        contenido: json['contenido'],
        fecha: json['fecha']);
  }
}
