import 'dart:core';
import 'dart:convert';

class Game {
  String idJuego = "";
  String titulo = "";
  String descripcion = "";
  List<dynamic> tags = [];
  List<dynamic> store = [];
  double puntuacion = 0.0;
  List<dynamic> multimedia = [];
  String portada = "";
  String fecha = "";
  String actualizacion = "";

  Game(
      {required this.idJuego,
      required this.titulo,
      required this.descripcion,
      required this.tags,
      required this.store,
      required this.puntuacion,
      required this.multimedia,
      required this.portada,
      required this.fecha,
      required this.actualizacion});

  factory Game.fromJson(Map<String, dynamic> json) {
    return Game(
        idJuego: json['_id'],
        titulo: json['Titulo'],
        descripcion: json['Descripcion'],
        tags: json['Tags'],
        store: json['TiendaId'],
        puntuacion: json['Puntuacion'].toDouble(),
        multimedia: json['Multimedia'],
        portada: json['Portada'],
        fecha: json['Fecha'],
        actualizacion: json['Actualizacion']);
  }

  String GamestoJSON() {
    var data = jsonEncode({"Puntuacion": puntuacion});
    return data;
  }
}
