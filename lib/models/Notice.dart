import 'dart:core';

class Notice {
  String title = "";
  String urlImage = "";
  String datetime = "";

  Notice({required this.title, required this.urlImage, required this.datetime});

  factory Notice.fronJson(Map<String, dynamic> json) {
    return Notice(
        title: json['Title'],
        urlImage: json['UrlImage'],
        datetime: json['Datetime']);
  }
}
