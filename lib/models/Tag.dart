import 'dart:core';

class Tag {
  String nombre = "";

  Tag({required this.nombre});

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(nombre: json['tag']);
  }
}
