import 'dart:core';

class Requirement {
  String idJuego = "";
  String minGrafica = "";
  String minProcesor = "";
  String minRam = "";
  String minAlmacen = "";
  String recomGrafica = "";
  String recomProcesor = "";
  String recomRam = "";
  String recomAlmacen = "";

  Requirement(
      {required this.idJuego,
      required this.minGrafica,
      required this.minProcesor,
      required this.minRam,
      required this.minAlmacen,
      required this.recomGrafica,
      required this.recomProcesor,
      required this.recomRam,
      required this.recomAlmacen});

  factory Requirement.fromJson(Map<String, dynamic> json) {
    return Requirement(
        idJuego: json['IdJuego'],
        minGrafica: json['minGrafica'],
        minProcesor: json['minProcesor'],
        minRam: json['minRam'],
        minAlmacen: json['minAlmacen'],
        recomGrafica: json['recomGrafica'],
        recomProcesor: json['recomProcesor'],
        recomRam: json['recomRam'],
        recomAlmacen: json['recomAlmacen']);
  }

  toList() {
    return {
      {
        "idJuego": idJuego,
        "graficos": minGrafica,
        "procesador": minProcesor,
        "ram": minRam,
        "almacenamiento": minAlmacen,
      },
      {
        "idJuego": idJuego,
        "graficos": recomGrafica,
        "procesador": recomProcesor,
        "ram": recomRam,
        "almacenamiento": recomAlmacen
      }
    };
  }
}
