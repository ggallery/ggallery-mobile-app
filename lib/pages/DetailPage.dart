import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/components/commentBox/commentBox.dart';
import 'package:ggallery/components/contentComments/ContentComments.dart';
import 'package:ggallery/components/contentRequirements/ContentRequirements.dart';
import 'package:ggallery/components/headerJuego/headerJuego.dart';
import 'package:ggallery/components/toolbar/Toolbar.dart';
import 'package:ggallery/models/Game.dart';
import 'package:ggallery/utils/Strings.dart';
import 'package:ggallery/views/Search/DescipctionGame/DescriptionGame.dart';

class Detail extends StatelessWidget {
  const Detail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final data = ModalRoute.of(context)!.settings.arguments as Game;

    String _portada = data.portada;
    return Scaffold(
      appBar: AppBar(
        title: Text(data.titulo),
        backgroundColor: Color(0xff23323F),
        leading: iconGgallery(context),
        titleTextStyle:
            TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Color(0xff23323F),
      body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(children: [
            HeaderJuego(_portada, data.multimedia[1],
                double.parse(data.puntuacion.toStringAsFixed(2))),
            DescriptionGame(data.titulo, data.descripcion, data.tags),
            ContentRequirements(
                data.idJuego), //No funciona el mapeoo de datos por el momento
            TitleComments(),
            ContentComments(data.idJuego),
            CommentBox(
                data,
                FirebaseAuth.instance.currentUser!.email
                    .toString()
                    .substring(0, 8),
                FirebaseAuth.instance.currentUser!.photoURL.toString(),
                data.idJuego), //Esnivio data prueba luego sera tomada desde el login del usuario
          ])),
    );
  }
}

Widget TitleComments() {
  return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          Strings.txtContentCommentsTitle,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ));
}
