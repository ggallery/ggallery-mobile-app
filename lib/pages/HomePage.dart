import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/components/carouselRecientes/RecientesView.dart';
import 'package:ggallery/components/listadoJuegosCategorias/ListarJuegos.dart';
import 'package:ggallery/components/listadoTiendas/ListarTiendas.dart';
import 'package:ggallery/components/toolbar/Toolbar.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Game.dart';
import 'package:ggallery/utils/Strings.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff23323F),
        titleTextStyle:
            TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        leading: iconGgallery(context),
        actions: [Toolbar()],
      ),
      backgroundColor: Color(0xff23323F),
      body: ListView(
        children: [
          Titles(Strings.txtHomeRecent),
          RecientesView(),
          Titles(Strings.txtHomePopular),
          ListarJuegos(),
          Titles(Strings.txtHomePopularStores),
          ListarTiendas()
        ],
      ),
    );
  }
}

Widget iconGgallery(context) {
  return GestureDetector(
      onTap: () => {Navigator.pushNamed(context, 'home')},
      child: Container(
        margin: EdgeInsets.all(7),
        height: 38,
        width: 38,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          image: DecorationImage(
              image: AssetImage('assets/ico/Logo_Ggallery.png'),
              fit: BoxFit.cover),
        ),
      ));
}

Widget Titles(String title) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
    child: Text(
      title,
      style: TextStyle(
          color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),
    ),
  );
}
