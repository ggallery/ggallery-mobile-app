import 'package:flutter/material.dart';
import 'package:ggallery/controllers/MapsController.dart';
import 'package:ggallery/models/PhysicalStore.dart';
import 'package:ggallery/views/maps/MapView.dart';

class MapPage extends StatelessWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MapController _mapController = MapController();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff23323F),
        title: Text("Tiendas Cercanas"),
      ),
      body: FutureBuilder(
          future: _mapController.getAllPhysicalStore(),
          builder: (context, AsyncSnapshot<List<PhysicalStore>> snapshot) {
            if (snapshot.hasData) {
              return MapView(snapshot.data!);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}
