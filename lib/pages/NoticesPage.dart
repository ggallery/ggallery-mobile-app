import 'package:flutter/material.dart';
import 'package:ggallery/components/toolbar/Toolbar.dart';
import 'package:ggallery/views/News/NewsView.dart';

class Notices extends StatelessWidget {
  const Notices({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xff23323F),
        titleTextStyle:
            TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        leading: iconGgallery(context),
        actions: [Toolbar()],
      ),
      backgroundColor: Color(0xff23323F),
      body: NewsView(),
    );
  }
}
