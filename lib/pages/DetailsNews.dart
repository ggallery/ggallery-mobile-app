import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ggallery/components/toolbar/Toolbar.dart';
import 'package:ggallery/models/News.dart';

class DetailsNews extends StatelessWidget {
  const DetailsNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as News;
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Color(0xff23323F),
          titleTextStyle:
              TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          leading: iconGgallery(context),
          actions: [Toolbar()],
        ),
        backgroundColor: Color(0xff23323F),
        body: new Container(
          child: new SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                header(args.imagen, args.titulo),
                SizedBox(
                  height: 10,
                ),
                description(args.contenido),
              ],
            ),
          ),
        ));
  }

  Widget header(imagen, titulo) {
    return Container(
        height: 220,
        width: double.infinity,
        child: Stack(children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 5, 0, 10),
            child: ClipRRect(
              child: ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                child: Image.network(imagen,
                    fit: BoxFit.cover, height: 220, width: double.infinity),
              ),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          Align(
              alignment: AlignmentDirectional.bottomCenter,
              child: Container(
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 10),
                  height: 90,
                  child: Text(
                    titulo,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.start,
                  ))),
        ]));
  }

  Widget description(contenido) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
        alignment: Alignment.center,
        width: double.infinity,
        child: Center(
          child: Text(
            contenido,
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
            textAlign: TextAlign.start,
          ),
        ));
  }
}
