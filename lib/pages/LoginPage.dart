import 'package:flutter/material.dart';
import 'package:ggallery/components/carucel/CarouselLogin.dart';
import 'package:ggallery/utils/Strings.dart';
import 'package:ggallery/views/login/LoginView.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        CarouselLogin(),
        FormL(),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[_terminos()],
        )
      ],
    );
  }

  Widget _terminos() {
    return Container(
        child: Center(
      child: TextButton(
        style: TextButton.styleFrom(
          textStyle: const TextStyle(fontSize: 12),
        ),
        onPressed: () {},
        child: Text(
          Strings.txtLoginTermsText,
          style: TextStyle(color: Colors.white70, fontWeight: FontWeight.bold),
        ),
      ),
    ));
  }
}
