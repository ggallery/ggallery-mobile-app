import 'package:flutter/material.dart';
import 'package:ggallery/components/FilterGames/FilterGames.dart';
import 'package:ggallery/models/Tag.dart';
import 'package:ggallery/components/toolbar/Toolbar.dart';
import 'package:ggallery/models/StoreMarkers.dart';
import 'package:ggallery/utils/Strings.dart';
import 'package:ggallery/views/maps/MapView.dart';

class Filters extends StatelessWidget {
  const Filters({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final data = ModalRoute.of(context)!.settings.arguments as Tag;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff193E50),
        title: Text("Filtro: " + data.nombre),
        titleTextStyle:
            TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      backgroundColor: Color(0xff23323F),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            children: [Expanded(child: FilterGames(data.nombre))],
          )),
    );
  }
}
