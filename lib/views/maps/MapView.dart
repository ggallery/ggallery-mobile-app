import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/models/PhysicalStore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.Dart';

class MapView extends StatefulWidget {
  List<PhysicalStore>? markerStore;
  MapView(this.markerStore);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  Completer<GoogleMapController> _controller = Completer();
  late Position _currentPosition;
  late BitmapDescriptor myIcon;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(64, 64)), 'assets/ico/mando.png')
        .then((onValue) {
      myIcon = onValue;
    });
  }

  //create markers
  final Set<Marker> markers = new Set();

  Set<Marker> createMarkers() {
    print('NOMBRE MARCADOR: ${widget.markerStore![0].nombre}');
    for (var i in widget.markerStore!) {
      var latitud = double.parse(i.ubicacion.split(',')[0].trim());
      var longitud = double.parse(i.ubicacion.split(',')[1].trim());
      markers.add(Marker(
        //adding markers
        markerId: MarkerId(i.nombre),
        position: LatLng(latitud, longitud), //position of marker
        infoWindow: InfoWindow(
          //popup info
          title: i.nombre,
          snippet: 'Direccion: ${i.direccion}',
          onTap: () async {
            return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return new AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(20.0))),
                      backgroundColor: Color(0xff23323F),
                      title: new Text(
                        '${i.nombre}',
                        style: TextStyle(
                            fontFamily: "Smash",
                            color: Colors.white,
                            fontSize: 30),
                      ),
                      content: new Container(
                        height: 190,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                alignment: Alignment.topLeft,
                                child: RichText(
                                    text: TextSpan(children: [
                                  WidgetSpan(
                                      child: Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                    size: 30,
                                  )),
                                  TextSpan(
                                      text: '${i.direccion}',
                                      style: TextStyle(
                                          fontFamily: "Smash",
                                          color: Colors.white,
                                          fontSize: 20.0)),
                                ]))),
                            SizedBox(
                              height: 13,
                            ),
                            Container(
                                alignment: Alignment.topLeft,
                                child: RichText(
                                    text: TextSpan(children: [
                                  WidgetSpan(
                                      child: Icon(
                                    Icons.pageview,
                                    color: Colors.white,
                                    size: 30,
                                  )),
                                  TextSpan(
                                      text: '${i.nombre}',
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () {
                                          launch(i.pagina);
                                        },
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold)),
                                ]))),
                            SizedBox(
                              height: 13,
                            ),
                            Container(
                                alignment: Alignment.topLeft,
                                child: RichText(
                                    text: TextSpan(children: [
                                  WidgetSpan(
                                      child: Icon(
                                    Icons.phone,
                                    color: Colors.white,
                                    size: 30,
                                  )),
                                  TextSpan(
                                      text: '${i.telefono}',
                                      style: TextStyle(
                                          fontFamily: "Smash",
                                          color: Colors.white,
                                          fontSize: 20.0)),
                                ]))),
                          ],
                        ),
                      ));
                });

            // Doesn't run
            Navigator.pop(context);
          },
        ),
        icon: myIcon, //Icon for Marker
      ));
    }
    return markers;
  }

  //get current position
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _determinePosition(),
        builder: (BuildContext context, AsyncSnapshot<Position> snapshot) {
          if (snapshot.hasData) {
            _currentPosition = snapshot.data!;
            return Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                markers: createMarkers(),
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                    target: LatLng(
                        _currentPosition.latitude, _currentPosition.longitude),
                    zoom: 16),
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
              ),
            );
          } else {
            if (snapshot.hasError) {
              return AlertDialog(
                content: Text(snapshot.error.toString()),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }
        });
  }
}
