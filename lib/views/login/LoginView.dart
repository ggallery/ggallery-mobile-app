import 'package:flutter/material.dart';
import 'package:ggallery/components/buttons/ButtonFacebook.dart';
import 'package:ggallery/components/buttons/ButtonGoogle.dart';
import 'package:ggallery/components/logo/Logo.dart';

//Entre el logo y terminos debe ir los botones
class FormL extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[SafeArea(child: Logo()), content()],
      ),
    ));
  }
}

Widget content() {
  return Container(
    child: Column(
      children: <Widget>[ButtonGoole(), ButtonFacebook()],
    ),
  );
}
