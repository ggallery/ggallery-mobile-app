import 'package:flutter/material.dart';
import 'package:ggallery/components/contentDescriptionJuego/ContentDescriptionJuego.dart';

class DescriptionGame extends StatefulWidget {
  String _name;
  String _description;
  List<dynamic> _tags = []; //resibe un lista de tags como parametros

  DescriptionGame(this._name, this._description, this._tags);

  @override
  _DescriptionGameState createState() => _DescriptionGameState();
}

class _DescriptionGameState extends State<DescriptionGame> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          children: [
            ContentDescriptionJuego(
                widget._name, widget._description, widget._tags)
          ],
        ));
  }
}
