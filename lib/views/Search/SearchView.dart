import 'package:flutter/material.dart';
import 'package:ggallery/components/SearchList/SearchList.dart';
import 'package:ggallery/components/filterList/FilterList.dart';

class SearchView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SearchViewState();
  }
}

class _SearchViewState extends State {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: [
            FilterList(),
            Expanded(
              child: SearchList(),
            )
          ],
        ));
  }
}
