import 'package:flutter/material.dart';
import 'package:ggallery/components/itemNews/ItemNew.dart';
import 'package:ggallery/models/News.dart';
import 'package:ggallery/controllers/NewsController.dart';

class NewsView extends StatefulWidget {
  @override
  _NewsViewState createState() => _NewsViewState();
}

class _NewsViewState extends State<NewsView> {
  NewsController _newsController = NewsController();
  @override
  Widget build(BuildContext context) {
    Widget FutureNews() {
      return FutureBuilder(
        future: _newsController.getAllNews(),
        builder: (context, AsyncSnapshot<List<News>> snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                print(snapshot.data![index].titulo);
                return ItemNew(
                    snapshot.data![index].titulo,
                    snapshot.data![index].imagen,
                    differenceBetweenDates(
                        DateTime.parse(snapshot.data![index].fecha),
                        DateTime.now()),
                    snapshot.data![index].contenido);
              },
            );
          } else {
            if (snapshot.hasError) {
              print(snapshot.error);
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureNews();
  }

  differenceBetweenDates(DateTime date1, DateTime date2) {
    Duration difference = date2.difference(date1);
    if (difference.inDays > 30) {
      return "Hace " + (difference.inDays % 30).toString() + " days ago";
    } else if (difference.inHours > 24) {
      return "Hace " + difference.inDays.toString() + " days";
    } else if (difference.inHours > 1) {
      return "Hace " + difference.inHours.toString() + " hours";
    } else if (difference.inMinutes > 1) {
      return "Hace " + difference.inMinutes.toString() + " minutes";
    } else {
      return "Hace " + difference.inSeconds.toString() + " seconds";
    }
  }
}
