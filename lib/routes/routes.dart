import 'package:flutter/material.dart';
import 'package:ggallery/pages/DetailPage.dart';
import 'package:ggallery/pages/HomePage.dart';
import 'package:ggallery/pages/NoticesPage.dart';
import 'package:ggallery/pages/SearchPage.dart';
import 'package:ggallery/pages/LoginPage.dart';
import 'package:ggallery/pages/MapPage.dart';
import 'package:ggallery/pages/DetailsNews.dart';
import 'package:ggallery/pages/FiltersPage.dart';

Map<String, WidgetBuilder> getAplicationRoutes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => Login(),
    'home': (BuildContext context) => Home(),
    'search': (BuildContext context) => Search(),
    'detail': (BuildContext context) => Detail(),
    'notices': (BuildContext context) => Notices(),
    'map': (BuildContext context) => MapPage(),
    'detailNews': (BuildContext context) => DetailsNews(),
    'filter': (BuildContext context) => Filters(),
  };
}
