import 'package:ggallery/models/Requirement.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class RequirementsController {
  DataProvider _dataProvider = DataProvider();

  Future<List<Requirement>> getRequirement(String id) {
    var url = UrlProvider.BASE_REQUIREMET
        .resolve(UrlProvider.BASE_REQUIREMET.path + "/$id"); //
    print(url);
    var data = _dataProvider.getData(url);
    print(data);
    return data.then((value) {
      List<Requirement> requirement = [];
      for (var item in value) {
        requirement.add(Requirement.fromJson(item));
      }
      print(requirement);
      return requirement;
    });
  }
}
