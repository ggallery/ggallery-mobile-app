import 'package:ggallery/models/PhysicalStore.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class MapController {
  DataProvider _dataProvider = DataProvider();
  Future<List<PhysicalStore>> getAllPhysicalStore() {
    var data = _dataProvider.getData(UrlProvider.BASE_MAP);
    return data.then((value) {
      List<PhysicalStore> physicalStore = [];
      for (var item in value) {
        physicalStore.add(PhysicalStore.fromJson(item));
      }
      return physicalStore;
    });
  }
}
