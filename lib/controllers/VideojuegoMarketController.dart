import 'package:ggallery/models/Store.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class VideojuegoMarketController {
  DataProvider _dataProvider = DataProvider();

  Future<List<Store>> getAllStores() {
    var data = _dataProvider.getData(UrlProvider.BASE_VIDEOJUEGOS_MARKETS);
    return data.then((value) {
      List<Store> stores = [];
      for (var item in value) {
        stores.add(Store.fromJson(item));
      }
      return stores;
    });
  }
}
