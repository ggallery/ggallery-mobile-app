import 'package:ggallery/models/News.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class NewsController {
  DataProvider _dataProvider = DataProvider();
  Future<List<News>> getAllNews() {
    var data = _dataProvider.getData(UrlProvider.BASE_NEWS);
    return data.then((value) {
      List<News> news = [];
      for (var item in value) {
        news.add(News.fromJson(item));
      }
      return news;
    });
  }
}
