import 'package:ggallery/models/Game.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class GamesController {
  DataProvider _dataProvider = DataProvider();
  Future<List<Game>> getAllGames() {
    var data = _dataProvider.getData(UrlProvider.BASE_GAMES);
    return data.then((value) {
      List<Game> games = [];
      for (var item in value) {
        games.add(Game.fromJson(item));
      }
      return games;
    });
  }

  Future<List<Game>> getOneGame(String id) {
    var url =
        UrlProvider.BASE_GAMES.resolve(UrlProvider.BASE_GAMES.path + "/$id");
    var data = _dataProvider.getData(url);
    return data.then((value) {
      List<Game> games = [];
      for (var item in value) {
        games.add(Game.fromJson(item));
      }
      return games;
    });
  }

  Future<List<Game>> getPopularGames() {
    var data = _dataProvider.getData(UrlProvider.BASE_POPULAR);
    return data.then((value) {
      List<Game> games = [];
      for (var item in value) {
        games.add(Game.fromJson(item));
      }
      return games;
    });
  }

  Future<List<Game>> getRecienteGames() {
    var data = _dataProvider.getData(UrlProvider.BASE_RECIENTES);
    return data.then((value) {
      List<Game> games = [];
      for (var item in value) {
        games.add(Game.fromJson(item));
      }
      return games;
    });
  }

  Future<List<Game>> getFilterGames(String filter) {
    var url = UrlProvider.BASE_GAMES
        .resolve(UrlProvider.BASE_GAMES.path + "/filtros/$filter");
    var data = _dataProvider.getData(url);
    return data.then((value) {
      List<Game> games = [];
      for (var item in value) {
        games.add(Game.fromJson(item));
      }
      return games;
    });
  }
}
