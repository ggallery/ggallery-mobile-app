import 'package:ggallery/models/Comment.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class CommitController {
  DataProvider _dataProvider = DataProvider();

  Future<List<Comment>> getAllComments(String id) {
    var url =
        UrlProvider.BASE_COMMIT.resolve(UrlProvider.BASE_COMMIT.path + "/$id");
    var data = _dataProvider.getData(url);
    return data.then((value) {
      List<Comment> comments = [];
      for (var item in value) {
        comments.add(Comment.fromJson(item));
      }
      return comments;
    });
  }

  Future<List<Comment>> postCommit(String dataPost) {
    var url = UrlProvider.BASE_COMMIT;
    var data = _dataProvider.postDataUrl(url, dataPost);
    return data.then((value) {
      List<Comment> comments = [];
      for (var item in value) {
        comments.add(Comment.fromJson(item));
      }
      return comments;
    });
  }
}
