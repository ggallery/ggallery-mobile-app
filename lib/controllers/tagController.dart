import 'package:ggallery/models/Tag.dart';
import 'package:ggallery/provider/Provider.dart';
import 'package:ggallery/provider/UrlProvider.dart';

class TagController {
  DataProvider _dataProvider = DataProvider();

  Future<List<Tag>> getAllTags() {
    var data = _dataProvider.getData(UrlProvider.BASE_TAG);
    return data.then((value) {
      List<Tag> tags = [];
      for (var item in value) {
        tags.add(Tag.fromJson(item));
      }
      return tags;
    });
  }
}
