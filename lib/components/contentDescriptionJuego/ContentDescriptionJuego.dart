import 'package:flutter/material.dart';
import 'package:ggallery/components/descriptionJuego/DescriptionJuego.dart';
import 'package:ggallery/components/descriptionJuego/NombreJuego.dart';
import 'package:ggallery/components/descriptionJuego/TagsJuego.dart';

class ContentDescriptionJuego extends StatefulWidget {
  String _name;
  String _description;
  List<dynamic> _tags = []; //resibe un lista de tags como parametros

  ContentDescriptionJuego(this._name, this._description, this._tags);

  @override
  _ContentDescriptionJuegoState createState() =>
      _ContentDescriptionJuegoState();
}

class _ContentDescriptionJuegoState extends State<ContentDescriptionJuego> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        NombreJuego(widget._name),
        DescriptionJuego(widget._description),
        SizedBox(
          height: 10,
        ),
        TagsJuego(widget._tags),
        SizedBox(
          height: 10,
        ),
      ],
    ));
  }
}
