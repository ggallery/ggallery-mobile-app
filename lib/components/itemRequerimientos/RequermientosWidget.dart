import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:ggallery/models/Requirement.dart';
import 'package:ggallery/utils/Strings.dart';
import 'dart:ui';

// ignore: must_be_immutable
class RequerimentsWidget extends StatefulWidget {
  List<Requirement> _requerimentsList;
  RequerimentsWidget(this._requerimentsList);
  @override
  State<StatefulWidget> createState() {
    return _RequerimentsWidgetState(_requerimentsList);
  }
}

class _RequerimentsWidgetState extends State<RequerimentsWidget> {
  final CarouselController _controller = CarouselController();
  List<Widget> _requerimentsListSliders = [];
  _RequerimentsWidgetState(List<Requirement> requerimentsList) {
    var test = requerimentsList[0].toList();

    for (var item in test) {
      Widget _slider = Container(
        child: Stack(
          children: <Widget>[
            ClipRect(
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: new Container(
                  width: 325.0,
                  padding: EdgeInsets.all(10),
                  decoration: new BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey.shade400.withOpacity(0.1),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.shade400.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Container(
                    child: Wrap(
                      children: [
                        Row(
                          children: [
                            Text(Strings.txtGraphic,
                                style: TextStyle(color: Colors.white)),
                            Flexible(
                              child: Text(item["graficos"],
                                  style: TextStyle(color: Colors.white)),
                            )
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Text(Strings.txtProcessor,
                                style: TextStyle(color: Colors.white)),
                            Flexible(
                                child: Text(item["procesador"],
                                    style: TextStyle(color: Colors.white)))
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Text(Strings.txtMemory,
                                style: TextStyle(color: Colors.white)),
                            Flexible(
                                child: Text(item["ram"],
                                    style: TextStyle(color: Colors.white)))
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Text(Strings.txtStorage,
                                style: TextStyle(color: Colors.white)),
                            Flexible(
                                child: Text(item["almacenamiento"],
                                    style: TextStyle(color: Colors.white)))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );

      _requerimentsListSliders.add(_slider);
    }
  }
  int _selected = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: [
              SizedBox(
                width: 33,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(Strings.txtLbRequeriments,
                    style: TextStyle(color: Colors.white, fontSize: 20)),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              SizedBox(
                width: 33,
              ),
              Flexible(
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: _selected == 0
                        ? Colors.grey.shade200.withOpacity(0.1)
                        : Colors.transparent,
                  ),
                  onPressed: () => _controller.animateToPage(0),
                  child: Text(Strings.txtMinRequeriments,
                      style: TextStyle(color: Colors.white)),
                ),
              ),
              Flexible(
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: _selected == 1
                        ? Colors.grey.shade200.withOpacity(0.1)
                        : Colors.transparent,
                  ),
                  onPressed: () => _controller.animateToPage(1),
                  child: Text(Strings.txtRecRequeriments,
                      style: TextStyle(color: Colors.white)),
                ),
              ),
            ],
          ),
          CarouselSlider(
            items: _requerimentsListSliders,
            options: CarouselOptions(
                viewportFraction: 1,
                height: 150,
                onPageChanged: (index, reason) {
                  setState(() {
                    _selected = index;
                  });
                }),
            carouselController: _controller,
          ),
        ],
      ),
    );
  }
}
