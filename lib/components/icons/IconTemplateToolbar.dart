import 'package:flutter/material.dart';

class IconTemplateToolbar extends StatelessWidget {
  final IconButton icono;
  const IconTemplateToolbar(this.icono, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: icono,
    );
  }
}
