import 'package:flutter/material.dart';

class IconTemplate extends StatelessWidget {
  final Icon icono;
  const IconTemplate(this.icono, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      /*      padding: EdgeInsets.symmetric(horizontal: 30),
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(width: 0.5, color: Color(0xFFDFDFDF)),
          left: BorderSide(width: 0.5, color: Color(0xFFDFDFDF)),
          right: BorderSide(width: 0.5, color: Color(0xFF7F7F7F)),
          bottom: BorderSide(width: 0.5, color: Color(0xFF7F7F7F)),
        ),
      ), */
      child: icono,
    );
  }
}
