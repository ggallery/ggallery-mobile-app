import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/models/Game.dart';

// ignore: must_be_immutable
class ItemPorJuego extends StatefulWidget {
  List<Game> _images;

  ItemPorJuego(this._images);

  List<Game> getImages() {
    return _images;
  }

  @override
  _ItemPorJuegoState createState() => _ItemPorJuegoState();
}

class _ItemPorJuegoState extends State<ItemPorJuego> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8),
      color: Color(0xff23323F),
      child: CarouselSlider(
        options: CarouselOptions(
          height: 150.0,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 5),
          viewportFraction: 0.4,
          //enlargeCenterPage: true,
          autoPlayCurve: Curves.linear,
        ),
        items: widget.getImages().map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: 130.0,
                margin: EdgeInsets.symmetric(horizontal: 1.0, vertical: 0),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(i.portada), fit: BoxFit.fill)),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, 'detail',
                        arguments: Game(
                            idJuego: i.idJuego,
                            titulo: i.titulo,
                            descripcion: i.descripcion,
                            tags: i.tags,
                            store: i.store,
                            puntuacion: i.puntuacion,
                            multimedia: i.multimedia,
                            portada: i.portada,
                            fecha: i.fecha,
                            actualizacion: i.actualizacion));
                  },
                ),
              );
            },
          );
        }).toList(),
      ),
    );
  }
}
