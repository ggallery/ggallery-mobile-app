import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ggallery/utils/Strings.dart';
import 'package:google_sign_in/google_sign_in.dart';

class ButtonGoole extends StatelessWidget {
  const ButtonGoole({Key? key}) : super(key: key);

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth =
        await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.grey),
            onPressed: () {
              signInWithGoogle().then((value) {
                if (value.user != null) {
                  Fluttertoast.showToast(
                          msg: "Login Success",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.green,
                          textColor: Colors.white,
                          fontSize: 16.0)
                      .then((value) => Navigator.pushNamedAndRemoveUntil(
                          context, "home", (route) => false));
                } else {
                  Fluttertoast.showToast(
                      msg: "Login Failed",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              });
            },
            child: Row(
              children: <Widget>[
                Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100.0),
                        image: DecorationImage(
                            image: AssetImage('assets/ico/Google_icon.png'),
                            fit: BoxFit.cover))),
                SizedBox(width: 5),
                Text(Strings.txtLoginGoogleLogin,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
              ],
            )));
  }
}
