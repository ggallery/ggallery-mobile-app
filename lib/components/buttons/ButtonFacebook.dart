import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ggallery/utils/Strings.dart';

class ButtonFacebook extends StatelessWidget {
  const ButtonFacebook({Key? key}) : super(key: key);

  Future<UserCredential> signInWithFacebook() async {
    // Trigger the sign-in flow
    final LoginResult loginResult = await FacebookAuth.instance.login();

    // Create a credential from the access token
    final OAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(loginResult.accessToken!.token);

    // Once signed in, return the UserCredential
    return FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.blue),
            onPressed: () async {
              signInWithFacebook().then((value) {
                if (value.user != null) {
                  Fluttertoast.showToast(
                          msg: "Login success",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.blue,
                          textColor: Colors.white,
                          fontSize: 16.0)
                      .then((value) => Navigator.pushNamedAndRemoveUntil(
                          context, "home", (route) => false));
                } else {
                  Fluttertoast.showToast(
                      msg: "Login failed",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                }
              });
            },
            child: Row(
              children: <Widget>[
                Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100.0),
                        image: DecorationImage(
                            image: AssetImage('assets/ico/Facebook_icon.png'),
                            fit: BoxFit.cover))),
                SizedBox(width: 5),
                Text(Strings.txtLoginFacebookLogin,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ))
              ],
            )));
  }
}
