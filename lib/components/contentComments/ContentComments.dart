import 'package:flutter/material.dart';
import 'package:ggallery/components/itemComentario/ItemComentario.dart';
import 'package:ggallery/controllers/CommitController.dart';
import 'package:ggallery/models/Comment.dart';

class ContentComments extends StatefulWidget {
  String _idJuego = "";
  ContentComments(this._idJuego);

  @override
  _ContentCommentsState createState() => _ContentCommentsState();
}

class _ContentCommentsState extends State<ContentComments> {
  //_ContentCommentsState(this._comments);
  CommitController _commitController = CommitController();

  @override
  Widget build(BuildContext context) {
    Widget FutureComments() {
      return FutureBuilder(
        future: _commitController.getAllComments(widget._idJuego),
        builder: (context, AsyncSnapshot<List<Comment>> snapshot) {
          if (snapshot.hasData) {
            return Container(
                height: 300,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    print(snapshot.data![index].userName);
                    return ItemComentario(
                        snapshot.data![index].userName,
                        snapshot.data![index].photo,
                        snapshot.data![index].rating.toDouble(),
                        snapshot.data![index].comment);
                  },
                ));
          } else {
            if (snapshot.hasError) {
              print(snapshot.error);
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureComments();
  }
}
