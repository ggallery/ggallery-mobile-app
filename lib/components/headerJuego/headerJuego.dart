import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:ggallery/components/ratingbar/Rating.dart';

class HeaderJuego extends StatefulWidget {
  String _portada;
  String _banner;
  double _rating;

  HeaderJuego(this._portada, this._banner, this._rating);

  @override
  _HeaderJuegoState createState() => _HeaderJuegoState();
}

class _HeaderJuegoState extends State<HeaderJuego> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 280,
      width: double.infinity,
      child: Stack(
        children: [
          ClipRRect(
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
              child: Image.network(widget._banner,
                  fit: BoxFit.cover, height: 180, width: double.infinity),
            ),
          ),
          Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: 200,
                width: (MediaQuery.of(context).size.width / 2.4),
                alignment: Alignment.bottomLeft,
                margin: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                  color: Color(0xff23323F),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Image.network(
                  widget._portada,
                  fit: BoxFit.fill,
                  height: 200,
                  width: (MediaQuery.of(context).size.width),
                ),
              )),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              margin: EdgeInsets.only(
                  right: 10,
                  left: (MediaQuery.of(context).size.width / 2.4) + 20,
                  top: 190),
              height: 100,
              width: (MediaQuery.of(context).size.width),
              child: Rating(widget._rating),
            ),
          )
        ],
      ),
    );
  }
}
