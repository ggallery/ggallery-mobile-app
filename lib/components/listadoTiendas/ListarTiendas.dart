import 'package:flutter/material.dart';
import 'package:ggallery/components/itemTiendas/ItemPorTienda.dart';
import 'package:ggallery/components/tags/Tags.dart';
import 'package:ggallery/controllers/VideojuegoMarketController.dart';
import 'package:ggallery/models/Store.dart';

// ignore: must_be_immutable
class ListarTiendas extends StatefulWidget {
  @override
  _ListarTiendasState createState() => _ListarTiendasState();
}

class _ListarTiendasState extends State<ListarTiendas> {
  VideojuegoMarketController _videojuegoMarketController =
      VideojuegoMarketController();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200,
        child: FutureBuilder(
          future: _videojuegoMarketController.getAllStores(),
          builder: (context, AsyncSnapshot<List<Store>> snapshot) {
            if (snapshot.hasData) {
              return ItemPorTienda(snapshot.data!);
            } else {
              if (snapshot.hasError) {
                print(snapshot.error);
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
