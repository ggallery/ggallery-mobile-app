import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ggallery/components/contentComments/ContentComments.dart';
import 'package:ggallery/controllers/CommitController.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Comment.dart';
import 'package:ggallery/models/Game.dart';
import 'package:ggallery/utils/Strings.dart';

class CommentBox extends StatefulWidget {
  Game _data;
  String _userName = "";
  String _photo = "";
  String _idjuego = "";

  CommentBox(this._data, this._userName, this._photo, this._idjuego);

  @override
  _CommentBoxState createState() => _CommentBoxState();
}

class _CommentBoxState extends State<CommentBox> {
  double _rating = 0.0;

  final commentText = TextEditingController();

  void dispose() {
    commentText.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _rating = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            message(context),
            Row(
              children: <Widget>[
                starIcon(context),
                comment(context),
                sendIcon(context),
              ],
            ),
          ],
        ));
  }

  Widget message(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10),
      width: double.infinity,
      height: 40,
      child: Text(
        Strings.txtOpinionGame,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
  }

  Widget starIcon(BuildContext context) {
    return Container(
      child: IconButton(
        onPressed: () => showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            backgroundColor: Color(0xff193E50),
            title: const Text(
              'Rate this game',
              style: TextStyle(color: Colors.white),
            ),
            content: RatingBar.builder(
                initialRating: 0,
                allowHalfRating: true,
                minRating: 0.5,
                itemBuilder: (context, _) =>
                    Icon(Icons.star, color: Colors.amber),
                itemSize: MediaQuery.of(context).size.width / 9,
                updateOnDrag: true,
                onRatingUpdate: (rating) {
                  _rating = rating;
                  print(_rating);
                }),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Cancel'),
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () => {print(_rating), Navigator.pop(context, 'OK')},
                child: const Text('OK'),
              ),
            ],
          ),
        ),
        icon: Icon(Icons.star),
        color: Color(0xFFC2C2C2),
        iconSize: MediaQuery.of(context).size.width / 10,
      ),
    );
  }

  Widget comment(BuildContext context) {
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width / 1.4,
      child: TextField(
        controller: commentText,
        decoration: InputDecoration(
            fillColor: Color(0xFF8E9BA1),
            filled: true,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
            hintText: "Agregar comentario",
            hintStyle:
                TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget sendIcon(BuildContext context) {
    return Container(
      child: IconButton(
        onPressed: () => {
          _SendCommint(),
          _showToast(context),
          commentText.clear(),
          FocusScope.of(context).unfocus()
        },
        icon: Icon(
          Icons.send_outlined,
          color: Color(0xFFC2C2C2),
          size: MediaQuery.of(context).size.width / 12,
        ),
      ),
    );
  }

  void _SendCommint() {
    var data = Comment(
        comment: commentText.text,
        rating: _rating,
        userName: widget._userName,
        photo: widget._photo,
        idjuego: widget._idjuego);

    CommitController _commitController = CommitController();
    _commitController.postCommit(data.toJSON());

    double newPuntuacion = (widget._data.puntuacion + _rating) / 2;

    /* var putData = Game(
        idJuego: widget._data.idJuego,
        titulo: widget._data.titulo,
        descripcion: widget._data.descripcion,
        tags: widget._data.tags,
        store: widget._data.store,
        puntuacion: widget._data.puntuacion,
        multimedia: widget._data.multimedia,
        portada: widget._data.portada,
        fecha: widget._data.fecha,
        actualizacion: widget._data.actualizacion); */

    Navigator.pushNamed(context, 'detail',
        arguments: Game(
            idJuego: widget._data.idJuego,
            titulo: widget._data.titulo,
            descripcion: widget._data.descripcion,
            tags: widget._data.tags,
            store: widget._data.store,
            puntuacion: newPuntuacion,
            multimedia: widget._data.multimedia,
            portada: widget._data.portada,
            fecha: widget._data.fecha,
            actualizacion: widget._data.actualizacion));
  }
}

void _showToast(BuildContext context) {
  final scaffold = ScaffoldMessenger.of(context);
  scaffold.showSnackBar(
    SnackBar(
      content: const Text('Comentario guardado'),
      action:
          SnackBarAction(label: 'Ok', onPressed: scaffold.hideCurrentSnackBar),
    ),
  );
}
