import 'package:flutter/material.dart';


// ignore: must_be_immutable
class Tag extends StatefulWidget {
  String _tag;

  Tag(this._tag);

  String getTag() {
    return _tag;
  }

  @override
  _TagState createState() => _TagState();
}

class _TagState extends State<Tag> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      height: 25,
      width: double.infinity,
      child: Text(
        widget.getTag(),
        textAlign: TextAlign.start,
        style: TextStyle(
            fontSize: 20,
            color: Color(0xffC5C3C0),
            fontWeight: FontWeight.bold,
            shadows: [
              Shadow(
                color: Colors.white.withOpacity(0.3),
                //x, y
                offset: Offset(0.0, 5.0),
                blurRadius: 9,
              ),
            ]),
      ),
    );
  }
}
