import 'package:flutter/material.dart';
import 'package:ggallery/models/News.dart';

class ItemNew extends StatefulWidget {
  String _title;
  String _urlImage;
  String _datetime;
  String _contenido;
  ItemNew(this._title, this._urlImage, this._datetime, this._contenido);

  @override
  _ItemNewState createState() => _ItemNewState();
}

class _ItemNewState extends State<ItemNew> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'detailNews',
              arguments: News(
                  titulo: widget._title,
                  imagen: widget._urlImage,
                  contenido: widget._contenido,
                  fecha: widget._datetime));
        },
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Container(
                    width: double.infinity,
                    child: Column(
                      children: <Widget>[
                        Image(image: NetworkImage(widget._urlImage)),
                        Container(
                            width: double.infinity,
                            color: Color.fromRGBO(196, 196, 196, 0.3),
                            child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 5),
                                child: Column(
                                  children: [
                                    Center(
                                        child: Text(widget._title,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15))),
                                    Container(
                                        width: double.infinity,
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Text(widget._datetime,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10)),
                                        )),
                                    SizedBox(
                                      height: 5,
                                    )
                                  ],
                                ))),
                      ],
                    )))));
  }
}
