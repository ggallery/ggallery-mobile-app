import 'package:flutter/material.dart';

class ItemComentario extends StatefulWidget {
  String _userName;
  String _photo;
  double _rating;
  String _comment;
  ItemComentario(this._userName, this._photo, this._rating, this._comment);

  @override
  _ItemComentarioState createState() => _ItemComentarioState();
}

class _ItemComentarioState extends State<ItemComentario> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(5),
          color: Color.fromRGBO(25, 62, 80, 0.68),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 3,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Container(
            margin: EdgeInsets.symmetric(vertical: 20.0),
            child: Column(children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  userData(widget._userName, widget._photo),
                  ratingData(widget._rating)
                ],
              ),
              SizedBox(
                height: 10,
              ),
              commentData(widget._comment)
            ])));
  }
}

Widget userData(_userName, _photo) {
  return Container(
    child: Row(
      children: <Widget>[
        new Container(
            width: 40.0,
            height: 40.0,
            decoration: new BoxDecoration(
                border: Border.all(color: Colors.green, width: 3),
                shape: BoxShape.circle,
                image: new DecorationImage(
                    fit: BoxFit.fill, image: new NetworkImage(_photo)))),
        SizedBox(width: 5),
        new Text(
          _userName,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        )
      ],
    ),
  );
}

Widget ratingData(_rating) {
  return Container(
    child: Row(
      children: [
        Text(_rating.toString(),
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
        Icon(Icons.star, color: Colors.yellow)
      ],
    ),
  );
}

Widget commentData(_comment) {
  return Center(child: Text(_comment, style: TextStyle(color: Colors.white)));
}
