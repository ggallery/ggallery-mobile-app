import 'package:flutter/material.dart';
import 'package:ggallery/components/itemFilter/ItemFilter.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Tag.dart';
import 'package:ggallery/controllers/tagController.dart';

class FilterList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FilterList();
  }
}

class _FilterList extends State {
  TagController _tagsController = TagController();
  @override
  Widget build(BuildContext context) {
    Widget FutureTags() {
      return FutureBuilder(
        future: _tagsController.getAllTags(),
        builder: (context, AsyncSnapshot<List<Tag>> snapshot) {
          if (snapshot.hasData) {
            return Container(
                height: 50,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, 'filter',
                              arguments:
                                  Tag(nombre: snapshot.data![index].nombre));
                        },
                        child: ItemFilter(snapshot.data![index].nombre),
                      );
                    }));
          } else {
            if (snapshot.hasError) {
              print(snapshot.error);
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureTags();
  }
}






/* 
return Container(
      color:  Color(0xff23323F),
      width: double.infinity,
      height: 50,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          ItemFilter(name: "Accion"),
          ItemFilter(name: "Aventura"),
          ItemFilter(name: "Supenso"),
          ItemFilter(name: "Indie"),
          ItemFilter(name: "Dark Souls"),
        ],
      )

    ); */