import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/models/Store.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class ItemPorTienda extends StatefulWidget {
  List<Store> _images;

  ItemPorTienda(this._images);

  List<Store> getImages() {
    return _images;
  }

  @override
  _ItemPorTiendaState createState() => _ItemPorTiendaState();
}

class _ItemPorTiendaState extends State<ItemPorTienda> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8),
      color: Color(0xff23323F),
      child: CarouselSlider(
        options: CarouselOptions(
          height: 200.0,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 5),
          viewportFraction: 0.4,
          //enlargeCenterPage: true,
          autoPlayCurve: Curves.linear,
        ),
        items: widget.getImages().map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: 130.0,
                margin: EdgeInsets.symmetric(horizontal: 1.0, vertical: 0),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(i.logo), fit: BoxFit.fill)),
                child: GestureDetector(
                  onTap: () {
                    launch(i.pagina);
                  },
                ),
              );
            },
          );
        }).toList(),
      ),
    );
  }
}
