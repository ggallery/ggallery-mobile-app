import 'package:flutter/material.dart';
import 'package:ggallery/components/contentDescriptionJuego/ContentDescriptionJuego.dart';
import 'package:ggallery/components/itemJuegoFilter/ItemJuegoFilter.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Game.dart';
import 'package:ggallery/pages/DetailPage.dart';
import 'package:ggallery/views/Search/DescipctionGame/DescriptionGame.dart';

class SearchList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchListState();
}

class _SearchListState extends State {
  GamesController _gamesController = GamesController();
  @override
  Widget build(BuildContext context) {
    Widget FutureGames() {
      return FutureBuilder(
        future: _gamesController.getAllGames(),
        //initialData: [],
        builder: (context, AsyncSnapshot<List<Game>> snapshot) {
          if (snapshot.hasData) {
            return GridView.count(
                childAspectRatio: (MediaQuery.of(context).size.height / 1000),
                crossAxisCount: 2,
                mainAxisSpacing: 5,
                children: snapshot.data!.map((game) {
                  return InkWell(
                    child: ItemJuegoFilter(
                        game.portada, game.puntuacion, game.titulo),
                    onTap: () {
                      Navigator.pushNamed(context, 'detail',
                          arguments: Game(
                              idJuego: game.idJuego,
                              titulo: game.titulo,
                              descripcion: game.descripcion,
                              tags: game.tags,
                              store: game.store,
                              puntuacion: game.puntuacion,
                              multimedia: game.multimedia,
                              portada: game.portada,
                              fecha: game.fecha,
                              actualizacion: game.actualizacion));
                    },
                  );
                }).toList());
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureGames();
  }
}
