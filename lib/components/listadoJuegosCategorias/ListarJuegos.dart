import 'package:flutter/material.dart';
import 'package:ggallery/components/itemJuegos/ItemPorJuego.dart';
import 'package:ggallery/components/listadoTiendas/ListarTiendas.dart';
import 'package:ggallery/components/tags/Tags.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Game.dart';

// ignore: must_be_immutable
class ListarJuegos extends StatefulWidget {
  @override
  _ListarJuegosState createState() => _ListarJuegosState();
}

class _ListarJuegosState extends State<ListarJuegos> {
  GamesController _gamesController = GamesController();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 235,
        child: FutureBuilder(
          future: _gamesController.getPopularGames(),
          builder: (context, AsyncSnapshot<List<Game>> snapshot) {
            if (snapshot.hasData) {
              return ItemPorJuego(snapshot.data!);
            } else {
              if (snapshot.hasError) {
                print(snapshot.error);
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
