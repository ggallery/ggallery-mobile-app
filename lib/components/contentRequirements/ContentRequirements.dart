import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:ggallery/components/itemRequerimientos/RequermientosWidget.dart';
import 'package:ggallery/controllers/RequirementsController.dart';
import 'package:ggallery/models/Requirement.dart';

class ContentRequirements extends StatefulWidget {
  String _idJuego = "";
  ContentRequirements(this._idJuego);

  @override
  _ContentRequirementsState createState() => _ContentRequirementsState();
}

class _ContentRequirementsState extends State<ContentRequirements> {
  RequirementsController _requirementsController = RequirementsController();
  @override
  Widget build(BuildContext context) {
    Widget FutureRequirement() {
      return FutureBuilder(
        future: _requirementsController.getRequirement(widget._idJuego),
        builder: (context, AsyncSnapshot<List<Requirement>> snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: RequerimentsWidget(snapshot.data!),
            );
          } else {
            if (snapshot.hasError) {
              print(snapshot.error);
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureRequirement();
  }
}
