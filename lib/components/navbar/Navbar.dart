import 'package:flutter/material.dart';
import 'package:ggallery/components/icons/IconTemplate.dart';

class Navbar extends StatelessWidget {
  const Navbar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff193E50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconTemplate(Icon(Icons.home, color: Colors.white, size: 38)),
          IconTemplate(Icon(Icons.search, color: Colors.white, size: 38)),
          IconTemplate(Icon(Icons.map, color: Colors.white, size: 38)),
          IconTemplate(Icon(Icons.article, color: Colors.white, size: 38)),
        ],
      ),
    );
  }
}
