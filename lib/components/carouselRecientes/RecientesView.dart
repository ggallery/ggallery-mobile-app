import 'package:flutter/material.dart';
import 'package:ggallery/components/juegosRecientes/CarouselRecientes.dart';
import 'package:ggallery/components/tags/Tags.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Game.dart';
import 'package:ggallery/utils/Strings.dart';

class RecientesView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RecientesView();
}

class _RecientesView extends State<RecientesView> {
  GamesController _gamesController = GamesController();
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 235,
        child: FutureBuilder(
          future: _gamesController.getRecienteGames(),
          builder: (context, AsyncSnapshot<List<Game>> snapshot) {
            if (snapshot.hasData) {
              return CarouselRecientes(snapshot.data!);
            } else {
              if (snapshot.hasError) {
                print(snapshot.error);
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
