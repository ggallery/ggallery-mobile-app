import 'package:flutter/material.dart';

class NombreJuego extends StatefulWidget {
  String _name;
  NombreJuego(this._name);

  @override
  _NombreJuegoState createState() => _NombreJuegoState();
}

class _NombreJuegoState extends State<NombreJuego> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(5),
          color: Color.fromRGBO(196, 196, 196, 0.3),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
            child: Text(widget._name,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold))));
  }
}
