import 'package:flutter/material.dart';

class DescriptionJuego extends StatefulWidget {
  String _description;
  DescriptionJuego(this._description);

  @override
  _DescriptionJuegoState createState() => _DescriptionJuegoState();
}

class _DescriptionJuegoState extends State<DescriptionJuego> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        alignment: Alignment.center,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(5),
          color: Color.fromRGBO(196, 196, 196, 0.3),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Center(
            child: Text(widget._description,
            textAlign: TextAlign.justify,
                style: TextStyle(color: Colors.white, fontSize: 18))));
  }
}
