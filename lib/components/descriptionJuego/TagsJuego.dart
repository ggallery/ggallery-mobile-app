import 'package:flutter/material.dart';
import 'package:ggallery/components/itemFilter/ItemFilter.dart';

class TagsJuego extends StatefulWidget {
  List<dynamic> _tags = []; //resibe un lista de tags como parametros
  TagsJuego(this._tags);

  @override
  _TagsJuegoState createState() => _TagsJuegoState();
}

class _TagsJuegoState extends State<TagsJuego> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget._tags
              .map((e) => Row(
                    children: [ItemFilter(e)],
                  ))
              .toList()),
    );
  }
}
