import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _logo(),
    );
  }

  Widget _logo() {
    return ClipRRect(
        child: Container(
      height: 150,
      width: 150,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          image: DecorationImage(
              image: AssetImage('assets/ico/Logo_Ggallery.png'),
              fit: BoxFit.cover)),
    ));
  }
}
