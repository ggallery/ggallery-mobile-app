import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ggallery/components/icons/IconTemplateToolbar.dart';

class Toolbar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ToolbarState();
  }
}

class _ToolbarState extends State {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        IconTemplateToolbar(IconButton(
            onPressed: () {
              Navigator.pushNamed(context, 'notices');
            },
            icon: Icon(Icons.import_contacts_outlined,
                color: Colors.white, size: 25))),
        SizedBox(width: 10),
        IconTemplateToolbar(IconButton(
            onPressed: () {
              Navigator.pushNamed(context, 'map');
            },
            icon:
                Icon(Icons.gps_fixed_outlined, color: Colors.white, size: 23))),
        SizedBox(width: 10),
        IconTemplateToolbar(IconButton(
            onPressed: () {
              Navigator.pushNamed(context, 'search');
            },
            icon: Icon(Icons.search_outlined, color: Colors.white, size: 25))),
      ],
    );
  }
}

Widget iconGgallery(context) {
  return GestureDetector(
      onTap: () => {Navigator.pushNamed(context, 'home')},
      child: Container(
        margin: EdgeInsets.all(7),
        height: 38,
        width: 38,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          image: DecorationImage(
              image: AssetImage('assets/ico/Logo_Ggallery.png'),
              fit: BoxFit.cover),
        ),
      ));
}
