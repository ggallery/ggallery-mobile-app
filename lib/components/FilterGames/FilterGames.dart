import 'package:flutter/material.dart';
import 'package:ggallery/components/itemJuegoFilter/ItemJuegoFilter.dart';
import 'package:ggallery/controllers/GamesController.dart';
import 'package:ggallery/models/Game.dart';

class FilterGames extends StatefulWidget {
  String _tag = "";

  FilterGames(this._tag);
  @override
  _FilterGamesState createState() => _FilterGamesState();
}

class _FilterGamesState extends State<FilterGames> {
  GamesController _gamesController = GamesController();

  @override
  Widget build(BuildContext context) {
    Widget FutureFilterGames() {
      return FutureBuilder(
        future: _gamesController.getFilterGames(widget._tag),
        //initialData: [],
        builder: (context, AsyncSnapshot<List<Game>> snapshot) {
          if (snapshot.hasData) {
            return GridView.count(
                childAspectRatio: (MediaQuery.of(context).size.height / 1000),
                crossAxisCount: 2,
                children: snapshot.data!.map((game) {
                  return InkWell(
                    child: ItemJuegoFilter(
                        game.portada, game.puntuacion, game.titulo),
                    onTap: () {
                      Navigator.pushNamed(context, 'detail',
                          arguments: Game(
                              idJuego: game.idJuego,
                              titulo: game.titulo,
                              descripcion: game.descripcion,
                              tags: game.tags,
                              store: game.store,
                              puntuacion: game.puntuacion,
                              multimedia: game.multimedia,
                              portada: game.portada,
                              fecha: game.fecha,
                              actualizacion: game.actualizacion));
                    },
                  );
                }).toList());
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      );
    }

    return FutureFilterGames();
  }
}
