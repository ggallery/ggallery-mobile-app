import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class Rating extends StatefulWidget {
  double _rating;
  Rating(this._rating);

  @override
  _RatingState createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRect(
          child: new BackdropFilter(
            filter: new ImageFilter.blur(sigmaX: 10, sigmaY: 20.0),
            child: new Container(
                height: 75,
                width: 250,
                padding: EdgeInsets.only(top: 10, bottom: 10, left: 10),
                decoration: new BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.grey.shade200.withOpacity(0.5)),
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Rating: ',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(widget._rating.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20)),
                          Center(
                              child: RatingBar.builder(
                                  ignoreGestures: true,
                                  initialRating: widget._rating,
                                  allowHalfRating: true,
                                  minRating: 0.5,
                                  itemBuilder: (context, _) =>
                                      Icon(Icons.star, color: Colors.amber),
                                  itemSize: 28.0,
                                  updateOnDrag: true,
                                  onRatingUpdate: (rating) => setState(() {
                                        widget._rating = rating;
                                      })))
                        ],
                      ),
                    )
                  ],
                )),
          ),
        ),
      ],
    );
  }
}
