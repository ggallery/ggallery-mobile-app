import 'package:flutter/material.dart';

//Packages
import 'package:carousel_slider/carousel_slider.dart';

class CarouselLogin extends StatefulWidget {
  @override
  _CarouselLoginState createState() => _CarouselLoginState();
}

class _CarouselLoginState extends State<CarouselLogin> {
  List<String> images = [
    "assets/img/fondo_login.jpg",
    "assets/img/fortnite.jpg",
    "assets/img/gta_v.jpg",
    "assets/img/red_dead_2.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff23323F),
      child: CarouselSlider(
        options: CarouselOptions(
          height: double.infinity,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 5),
          viewportFraction: 0.8,
          enlargeCenterPage: true,
          autoPlayCurve: Curves.linear,
        ),
        items: images.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 1.0, vertical: 0),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(i), fit: BoxFit.fill)),
              );
            },
          );
        }).toList(),
      ),
    );
  }
}
