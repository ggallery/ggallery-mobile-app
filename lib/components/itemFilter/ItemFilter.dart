import 'package:flutter/material.dart';

class ItemFilter extends StatelessWidget {
  final String name;
  ItemFilter(this.name);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        padding: const EdgeInsets.only(left: 15.0, right: 15.0),
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(15),
          color: Colors.white38,
          boxShadow: [
            BoxShadow(
              color: Colors.black,
              spreadRadius: 1,
              blurRadius: 16,
              offset: Offset(1, 1), // changes position of shadow
            ),
          ],
        ),
        child: Center(
            child: Container(
          child: Text(
            name,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        )),
      ),
    );
  }
}
