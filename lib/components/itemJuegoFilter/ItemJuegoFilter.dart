import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ItemJuegoFilter extends StatefulWidget {
  String _image;
  double _rating;
  String _title;

  ItemJuegoFilter(this._image, this._rating, this._title);

  String getImage() {
    return _image;
  }

  double getRating() {
    return _rating;
  }

  String getTitle() {
    return _title;
  }

  @override
  State<StatefulWidget> createState() => _ItemJuegoFilter();
}

class _ItemJuegoFilter extends State<ItemJuegoFilter> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 300,
        width: 200,
        child: Column(
          children: [
            imagenJuego(widget.getImage()),
            tituloJuego(widget.getTitle())
          ],
        ));
  }

  Widget imagenJuego(img) {
    return Container(
      height: 195,
      width: 150,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      margin: EdgeInsets.all(10),
      child: Stack(
        children: [
          Image.network(
            img,
            fit: BoxFit.cover,
            height: 190,
            width: 150,
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: ratingJuego(widget.getRating().toStringAsFixed(2)),
          )
        ],
      ),
    );
  }

  Widget ratingJuego(rating) {
    return Container(
      height: 22,
      width: 45,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.circular(15),
        color: Color(0xff23323F),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Center(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            rating.toString(),
            style: TextStyle(color: Colors.white),
          ),
          Icon(
            Icons.star,
            color: Colors.yellow,
            size: 17,
          )
        ],
      )),
    );
  }

  Widget tituloJuego(title) {
    return Container(
      padding: EdgeInsets.all(10),
      width: 150,
      height: 36,
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.circular(15),
        color: Color(0xff45484A),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Center(
          child: Text(
        widget.getTitle(),
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.white),
      )),
    );
  }
}
