class Strings {
  //Login texts strings
  static String txtLoginFacebookLogin = "INICIAR SESIÓN CON FACEBOOK";
  static String txtLoginGoogleLogin = "INICIAR SESIÓN CON GOOGLE";
  static String txtLoginTermsText =
      "Al registrarse esta aceptando los terminos y condiciones";

//Home texts strings
  static String txtHomeRecent = "Recientes";
  static String txtHomePopular = "Populares";
  static String txtHomePopularStores = "Tiendas Destacadas";
  static String txtFilterGames = "Videojuegos filtrados por: ";

  //ContentComments
  static String txtContentCommentsTitle = "Calificaciones y opiniones";

//Details Game texts strings
  static String txtOpinionGame = "Escribe una opinión";
  static String txtHintComment = "Agregar comentario";

  static String txtMinRequeriments = "MINIMOS";
  static String txtRecRequeriments = "RECOMENDADOS";
  static String txtLbRequeriments = "Requerimientos";

  static String txtProcessor = "Procesador: ";
  static String txtGraphic = "Gráficos: ";
  static String txtMemory = "Memoria: ";
  static String txtStorage = "Almacenamiento: ";

}
